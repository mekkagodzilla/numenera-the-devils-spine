# Numenéra - The Devil's Spine - Session 4 : Sur la route d'Harmuth

## Résumé de l'épisode précédent

Les PCs ont réussi détruire l'appareil de transmission qui permettait au Chœur Insidieux de communiquer de corps en corps et de maintenir sa cohésion.
En chemin, ils ont rencontré plusieurs personnnages qui les ont assisté dans leur mission :

- Theobald le guide : Lv3, Lv5 pour la géographie et les connaissances de la nature, 1 PA, 12 PV
- Askalan : aventurière du genre tête brûlée et avide. Lv4, Lv5 speed def, 2 PA, 18 PV, +2 cyphers
- Ederana : Lv2, Lv4 dans toutes les intéractions, 1 PA, amie de Dro qu'elle est venue sauver
- Dro : Lv3, Lv4 speed def, 2 PA, +1 dgt avec son épée (Lv4 artefact, Depl 1in1d20)
- Rion: Lv5, 3 PA, 20 PV. C'est un Glaive de Shallamas. Animé par la vengeance.


## Premier embranchement : prendre la rivière au plus près d'Uxphon ou pousser jusqu'à Charmonde, capitale de Navarène

### les PCs arrivent à un petit village sur la rivière Tithe, et y trouvent de frèles esquifs
Les villageois n'utilisent pas ses radeaux de fortune comme navires, mais pour mettre à l'abris leur nourriture d'une créature qu'ils nomment la "Bouche Cachée".
Les villageois leur expliquent que la Bouche Cachée est un être invisible qui mange tout ce qu'il trouve, et que deux enfants du village ont été dévorés (seules leurs chausses ont été retrouvées, ensanglatées).

Les cheffes du village, deux femmes âgées appelées Rhotia et Sulucu, peuvent être persuadées de préter un radeau aux PCs, mais la meilleure manière est de débarasser le village de la "Bouche Cachée".

**Rhotia et Sulucu**: Lv3 (ensemble), Lv4 pour défense intelligence

La "Bouche Cachée" est une créature appelée un Balikna, qui est particulièrement douée en camouflage. Quasi invisible de jour, elle chasse la nuit.

**Balikna, Bestiary 1 p23**: Lv 4, 20 PV, 3 PA, 6 dgts. Mouvements : courte distance. 
Défend comme Lv 5 grâce à camouflage.
Si le balikna est caché, les PCs doivent réussir un lancer d'int chaque round pour le voir. Echec : on ne peut pas l'attaquer, et défendre est +2 difficile.
Son attaque si caché : 6 dégats + étourdi prochain tour.

**GM intrusion** : le Balikna donne un coup de queue à un PC qui n'a pas réussi à le détecter, le rendant inconscient pendant 1d4 rounds.

### Si les PCs vont directement à Charmonde

À Charmonde, les PCs n'ont pas le temps de faire du tourisme, mais peuvent se ravitailler, et enquêter sur la lame impossible.

#### Boutique : Chez Tolarus, armes et protections
**Tolarus, un marchand costaud qui aime trop les explosions.**
Tolarus est un humain immense, dont une partie du corps est remplacée par des greffes mecaniques. La moitié de son visage et son bras droit sont entièrement mécaniques.
Il propose :

- des armes de jet légères (arbaletes) et moyennes (arc longs), 
- des munitions
- des cyphers : 
  - cristal explosif (1d6 +3 à courte distance, + 4 dgts ignorant l'armure à longue distance)
  - Explosif electromagnétique : sphère en céramique. Après explosion, libère terribles ondes électro magnétique : appareils numénéra neutralisés pendant 1 heure.

#### Nimeta, vendeuse de cyphers à la sauvette
**Nimeta, une marchande discrète qui trafique dans les allées sombres.**

Dans les rues de Charmonde, les PCs tomberont sur **Nimeta**, qui leur fera un clin d'œil appuyé et les invitera à la suivre.
Elle fait dans le commerce illégal de Cypher. Elle opère principalement par troc de cyphers + peut demander des services.
C'est une humaine aux cheveux et à la peau très foncés, qui paraît avoir 20 ans à peine.

Elle a en stock :

- Mini gate Lv5 : sphère de métal. Quand activée, mini portal au poignet, la main apparaît au bout d'un autre portail à longue distance, mais visible.
- détecteur de vie (lunettes) : révèle la position exacte de toute créature vivante à portée. Toutes les minutes, le porteur lance un d6, sur un 1, l'appareil est désactivé.
- Reset (amulette) : activée, cette amulette efface les 5 dernières minutes. Seul le PC qui a activé le reset se rappelle des 5 minutes qui ont été effacées. 
- Lutin (Light drops) Lv7 : bracelet, projecteur, longue portée. Libère un nuage de billes de lumière, courte distance. Le nuage dure 1d6 rounds. Effet d100 : 01-50 blind all. 51-75 : Sleep pour LvxRounds, 76-100 : Dazed all, tasks hindered.
- Speed Head, Lv7, pillule. Restaure 1 Might chaque minute pendant 1h. Utile pour dgts sur la durée.
- Heat sensor Lv4 : Pill, ou verres de contact, lunettes. Pendant une heure, perception des objets via leur température. Permet de voir l'invisible, ou même des objets dans d'autres objets, si température très différentes.

Service demandé en échange de 2 cyphers : un junkie du genre violent l'agresse quand elle essaie de vendre ces cyphers dans la rue. Il squatte le quartier et est facile à reconnaitre avec sa coupe de cheveux et son air de camé.

**Serv, un camé sûr de lui qui aggresse les faibles.**
Lv4, 2 PA, 15 PV, Lv3 en speed def, 4 dgts avec son canif, Lv5 en intéractions.
Sur lui :

- Time dilatation Nodule Lv1 : cristal attaché à une arme de CàC. Pendant 28h, l'utilisateur de l'arme se déplace à une vitesse surnaturelle (pour les autres), et ses attaques sont facilitées de 2 crans.
- Frigid wall projector Lv8 : crée un mur d'air super réfrigéré de 9m x 9m x 30cm. Inflige dgts = Lv à qui le traverse. Dure 10mn.

**GM intrusion** : Serv utilise un de ses cyphers et attaque dans le même round.

#### Frère Joannic et "ses" enfants
**Frère Joannic, un jovial nano qui cache bien son jeu**
Lv5. 0 PA, 15 PV, 3dgts. Défend comme Lv6, physique comme mental. Utilise son power glove. 

Si les PCs vont voir du côté du cloître des Prêtres des Âges, ils rencontreront **Frère Joannic**.  
Visage Jovial, barbe fournie, bonne bedaine, grand rire franc.

Il est accompagné de ses 2 protégés, des enfants appelés **Simon et Cybille**, qui ont environ 8 ans. Ils semblent avoir une immense maturité et de grandes connaissances. Les enfants sont des jumeaux, ont l'air timides.  
Joannic les présente comme ses assistants, ses protégés, ses enfants.

Joannic a de grandes connaissances en Numenéra, et sait que l'ermite Nevajin appelé Eenosh prétend savoir où en trouver un exemplaire. Il utilisera cette connaissance comme monnaie d’échange contre la protection du groupe sur la route fluviale. Il souhaite déposer les enfants chez leur tante sur la côte le temps la croisade au nord soit réglée.

**GM intrusion** : Joannic propose aux PCs humains de leur acheter Hector.

## Sur la rivière

### Si les PCs ne sont pas avec Joannic 
Après quelques jours de navigation tranquille, leur radeau est attaqué par un Morl (Bestiary 1, p 87)
**GM intrusion** : La créature semble constituée de fluide, elle se glisse entre les planches du radeau et d'un coup brise la frêle embarcation. 

**Morl, Bestiary p87** : Lv5, 0 PA, 30 PV, dgts 5. Df physique comme Lv6, dgts mentaux comme Lv3.

**GM Intrusion** : un PC tranche un tentacule du Morl, qui prend vie et l'attaque (Lv3).

Après le combat, les PCs croisent le bâteau de Joannic, qui leur propose son aide.

### Dans le bâteau de Joannic

Les PCs prennent leur quartier dans la chambre des enfants, qui vont dormir avec Joannic le temps du trajet.

Dans la chambre des enfants, les PCs observateur trouveront un livret rempli de calculs très compliqués, et le journal intime de Cybille.

Cybille est très angoissée par l'intelligence surnaturelle qu'ont son frère et elle. Elle décrit aussi le traitement violent que leur inflige souvent Joannic.  
Joannic leur pose sans cesse des questions, étudie leur poulps et leur températures après de longs interrogatoires, a tenté des séances d'hypnose sur eux.  
Elle se demande s'ils sont les sujets d'une expérience scientifique pour Joannic.

Du côté de Simon, un autre carnet rempli de calculs et de plans de machines qui ont l'air beaucoup trop complexes pour être le fruit d'un enfant de 8 ans.
 
Dans la chambre de Joannic, s'ils y entrent (dif 5):

- 50 shins
- Power Glove Artefact Lv4 : un grand en métal surdimensionné avec des pointes aux jointures des phalanges. Si simplement porté, attaques à main nue +1dgt. Si activé (1 action), attaques +5 dgts + poussent cible touchée une distance courte. 
- Slave cap lv3.

Dans son journal, il y a des comptes rendu d'expériences qu'il a mené sur les enfants. Il cherche à comprendre d'où vient leur intelligence. Pourquoi résistent-ils au slave cap ? Quels sont ces plans qu'ils dessinent ?

**GM intrusion** : Joannic tente d'entrer dans sa chambre au moment où un PC fouille son contenu. S'il y arrive, il tente de mettre la courronne d'esclave sur la tête du PJ intrusif pour le faire sortir sans douleur, et lui administre une pillule d'oubli.


## En cours de route

Alors que le bateau passe à la hauteur du monolithe de marbre, Joannic invite les PCs à se recueillir en souvenir de Calaval, le premier pape, qui a exploré ce monolithe 4 siècles auparavant, et s'est retrouvé haut dans le ciel, au dessus de la terre.

Alors un automate étrange apparaît à l'horizon et semble venir droit sur eux. C'est un **Disassembler**.

**Disassembler, Discovery p233** : Lv5, 4 PA, 18 PV, Dgts 5, mouv. court.
Loot : 80 shins, 6 cyphers (random from telegram bot)

Machine intelligente mais erratique. Joannic ne semble pas inquiet car il en a déjà croisé plusieurs et a toujours réussi à raisonner avec eux.
Mais après avoir scanné les enfants, ce Disassembler ne veut rien entendre et détermine que les enfants doivent lui être confiés immédiatement.
Les PCs peuvent le combattre, ou éventuellement le persuader de se joindre à eux pour le moment.

**GM intrusion** : le Disassembler attaque soudainement sans prévenir.

## La fin du voyage

Soudain apparaissent à l'horizon un large troupeau d'**Ithsyns Discovery P237**.
Il font un barouf d'enfer. 

Simon a pris la barre un peu avant, et semble paniquer. Il dirige maladroitement le bateau vers la berge.

3 Ithsyns attaquent.

**Ithsyn** : Lv4, 1 PA, 12 PV, Dgts 4. Perception Lv3, Résiste attaques mentales Lv3.
Si touché, libère un gaz, might def roll, ou bien effet aléatoire (voir table p237)

**GM intrusion** prolonger les effets du gaz pendant 1d6 rounds.

Pendant le combat, Joannic, Simon et Cybille ont disparu.
Leurs traces menent à la forêt proche.

## Dans la Forêt de Westwood.

**Culova, Discovery p231** : Lv4, 2 PA, 20 PV, long mouvement. Discrétion et Grimpette : Lv6.

Sur les traces de Joannic et des enfants, les PCs s'enfoncent dans la forêt. Ils commencent à trouver de grandes toiles d'araignées, de plus en plus nombreuses.
La trace de Joannic est facile à suivre, il semble avoir coupé pas mal de toiles et laissé une Culova morte derrière lui.

Décrire le corps de cette créature : 8 pattes pointues, abdomen gonflé, torse quasi humain avec 2 bras, tête ornées de 8 yeux, grande bouche.

Un groupe de 6 culovas arrive, et en voyant le corps de leur congénère abattue, se mettent en formation d'attaque.
Elles s'adressent au PCs dans un language humain, les accusant d'avoir tué une des leurs.

Tâche Lv5 de les convaincre de se joindre à eux.

## Final showdown avec Joannic

Les PCs aperçoivent Joannic dans une clairière. Il est occupé à manipuler un ensemble de symboles sur une grande dalle qui semble faite de pierre. Il semble agîté et énervé, il est blessé et n'arrive pas à faire ce qu'il veut semble-t-il. Simon et Cybille ont l'air épuisés et éteints, ils sont assis derrière Joannic.

Après quelques secondes, Joannic parvient finalement à ouvrir une porte de lumière bleue et s'apprête à porter les enfants sur ses épaules et à la traverser.

**GM Intrusion** : Joannic frappe violemment un PC à la tempe, la victime est sonnée et saute son prochain tour.

Pendant le combat, au moment opportun, Cybille révèle son troisième œil et tire un rayon rouge mortel sur Joannic.

## Arrivée à Harmuth

Les PCs sont accueillis par l'odeur de pulpe de bois humide et fermentée, et la vision de moulins à papier.
Ils peuvent croiser des humains qui volent sur des troncs d'arbres, de la forêt vers les moulins.
Un jeune homme qui semble piloter maladroitement manque de toucher un PJ.
S'ils le traitent gentiment, il leur offrira un cypher ou 2 pour se faire pardonner. (piocher dans ceux pas utilisés ou tirer avec le bot telegram)

Arrivées à la ville proprement dite, les PJs réalise que c'est une ville moyenne, mais qui autrefois devait être bcp plus vaste, une grande partie est immergée par la mer dont le niveau a monté de plusieurs mètres.

Les quais sont animés par le va et vient de bateaux de pêche. Dans les rues, il y a énormément de librairies, et d'éditeurs, et de marchands de papier et d'encres.
Il y a même 2 journaux locaux.

Les gros titres sont sur 3 thèmes principaux :

- Les tempêtes de plus en plus nombreuses au large
- Le festival du Poisson qui arrive bientôt
- Une vague de cambriolages sur la ville.

Rappel : le PNJ que les PJ doivent trouver s'appele Cagyn.
