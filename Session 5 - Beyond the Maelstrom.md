# Numenéra - The Devil's Spine - Session 5
# Beyond the Maelstrom

## préambule :

- Rappel des choix du second tier faits par joueurs, + de leurs capacité de Focus.
- Rappel de la règle sur l'armure : réduction dégâts, augmentation coût effort speed, respectivement [1, 2, 3] pour [légère, moyenne, lourde].

## Résumé de l'épisode précédent

Beren, Épicure et Hector, sur la route d'Harmuth, et en quête de renseignements sur la lame impossible, tombent sur un prêtre des âges initialement sympathique mais qui menait une série d'expériences plutôt traumatisante sur 2 enfants extrèmement intelligents.
Après quelques péripéties à bord d'un petit voilier, nos héros arrivent enfin à Harmuth.

Rappel des infos données par Devola : les PCs ont besoin pour leur chirurgie d'une substance chimique appelée **gharolan**.
Le **Gharolan** est sécrétée par des créatures extrémophiles appelées **Celosias**, vivant dans les fonds marins, dans les évent thermiques.

Elle a indiqué aux PNJs d'aller à Harmuth et d'y trouver **Cagyn Rilin**. Lui et Devola se sont déjà rencontrés.

## Flow général de l'aventure

1. Les PCs arrivent à harmuth. Ils peuvent faire plusieurs petites rencontres et s'équiper pour l'exploration des fonds marins.
2. Puis ils doivent chercher **Cagyn Rilin** et demander son aide. Il leur indique où sont les évents, un objet pour pointer la direction (perdu) et quel matériel se procurer.
3. Les PCs doivent trouver un moyen de transport (plusieurs options fournies).
4. Une fois en mer, il leur faut 6 jours pour arriver à destination. Rencontres proposées pages 73 & 74.
5. Les PCs voient un gros navire au loin. Le navire est en fait un village juché sur le dos d'une espèce d'animal marin géant. Voix télépathique qui les accueille avec un "Bienvenue".
6. rencontre PNJ Shon'ai. C'est une scientifique qui a fait construire ce village / bateau et étude les animaux marins. Elle dit venir du Sabeta, contrée inconnue dans le steadfast. Elle a repéré au fond une structure intéressante, un château constitué d'eau, mais n'a pu l'explorer à cause du Maelstrom.
7. Descente dans les profondeurs
8. L'antre du Moyag - Dungeon crawl / fights
9. Conclusion


### Misc Quests in Harmuth

#### the Missing Twins :

2 enfants qui sont censés être les mascottes de la fête du poisson ont disparu, un gendarme les cherche, et suspecte les PCs.
Les gamins sont trouvables dans la vieille ville. Le gendarme essaie d'étouffer la révélation qu'il n'y avait pas de kidnappeur.
Le gendarme : Maréchal Eltre

#### A ring on each finger

Grande et élégante femme. Juleen Rowley.
A perdu ses dix bagues d'un coup.
Promet récompense.
Petit animal ressemblant à un crapeau, à la langue préhensile, et faisant un nid dans les arbres.

Les PCs peuvent trouver 11 anneaux.
Elle leur offre 3 shins par anneau + elle peut leur obtenir un bateau au port.


#### Spoonful of medecine
Les PCS peuvent trouver un type appelé Frund qui en a. Mais il refuse de le vendre et en a une quantité très faible, pour sa femme.

### Cagyn Rilin

Tout PNJ à Harmuth peut les informer que :

- Cagyn est un peu farfelu / fou / dérangé
- Il vit sur une péniche bleue (facile à trouver, c'est la seule bleue), en centre ville
- Il a une espèce de rituel où il s'assoit dans l'eau avec des cables accrochés aux oreilles pendant des heures.
- il est farfelu mais pas détesté

Quand les PCs approchent, il est assis dans l'eau, jusqu'au cou, avec les tubes et une boîte synth en main. 
Grand, trappu, barbu. Il n'est pas emballé à l'idée d'être interrompu dans son activité.

Il est nu à part les cables autour de son corps. Sa maison est plein de cables divers, ext et intérieur.
Certains cables ont l'air artificiels, d'autres ont l'air de pousser et de vivre.
Il est un expert cartographe de la mer des secrets, et peut montrer aux PCs une carte holographie, montrant où se trouvent les évents thermaux.  
Il prévient les PCs qu'il y a un danger, une grande tempête qui se prépare.   
Il avait un objet utile, un anneau qui indique une position pré-programmée, mais il l'a perdu. Voir side quest **Un anneau pour chaque doigt**  
Cagyn ne veut pas les accompagner, il ne sait pas nager et a une peur phobique des profondeurs.  

## Lieux
### Harmuth
Harmuth est une ville côtière, ancienne, et partiellement engloutie.  
Ses industries sont la pêche et le papier.


### en Mer en suivant instructions de Cagyn

Les PCS sont abordés par téléphatie par un immense batiment flottant.  
C'est un village sur le dos d'une espèce d'animal marin géant.

À bord, des humains étranges qui parlent à peine la Vérité.  
Leur leader, Shon'ai, parle mieux et accueille les PCs.

C'est une scientifique des fonds marins. Elle sait où se trouvent les évents sousmarins.  
Elle sait aussi le danger qui rôde mais n'est pas sûre des PCs.

Si convaincue peut proposer son aide en se transformant en enveloppe corporelle pour un PC.

### Profondeurs 

cf p 76
