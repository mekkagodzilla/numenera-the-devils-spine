# Numenéra Session 6 - The Royal Tomb

## Trouver Eennosh, le Nevajin

Les joueurs ont entendu parler d’Eenosh, un sage qui connaîtrait l’emplacement d’une lame impossible.

Plusieurs NPCs sont capable d’indiquer l’emplacement de son antre, env. 100 km à l’ouest d’Uxphon.  
C’est un désert.  
Petite hutte dans une zone entourée de rochers.

Il est capturé par une bande de 6 chirogs, qui le forcent à leur donner des tuyaux sur l’environnement de ce désert, la chasse et la survie.  
Les Chirogs ont construits des yourtes de fortune autour de la hutte d’Eenosh, et s’approchent dès que les PCs arrivent.

**Chirog** : *Lvl 4, Lv7 pour grimpette, 15 PV, 3 armure, attrappe et immobilise son ennemi (auto échec défense, test force pour se libérer).*

- Si attaqués, ou menacés : ultra violence immédiate.
- Si apaisés : ils sont ammenés devant Eenosh, car il peut leur parler mais les Chirogs ne comprennent pas la Vérité™.

Eenosh fait semblant de rudoyer les PCs mais leur demande explicitement leur aide pour se libérer des Chirogs.
"Sortez moi de là SVP !"

Si libéré, il offre 2 cyphers :

- Pilulle qui soigne 5 points Might Pool
- Pilulle qui donne +1 Intell Edge pendant 1 heure

Il donne aussi des explications :

- La lame impossible est située dans la tombe de la Reine Starloscet, une reine humaine morte il y a plus de 100 ans 
- Il leur indique d’aller à une communauté étrange appelée Barrow-Town où ils trouveront ce qui leur faut
- Il leur conseille d’apprendre ce qu’ils peuvent sur les légendes autour de la tombe, car certaines légendes sont vraies

Direction ouest pour 2 jours de marche pour rejoindre Barrow-Town.

## Barrow-Town

Barrow-Town est une ruine habitée. Personne ne sait exactement comment cette communauté a été créée.

C’est un amalgame de métal, synth, et de pierre récupérées dans les gravats d’une ruine ancienne.

Les habitations sont juchées loin au-dessus du sol, adossées à une structure d’un ancien monde.

Il y a environ 450 habitants, gouvernés par un conseil des anciens. Aucune présences des Prêtes des Âges.

### Le voleur de tombeaux

Les PCs seront amenés à croiser **Buris Han** s’ils font savoir aux PNJs qu’ils cherchent la lame impossible ou bien simplement à se rendre dans la Tombe de la Reine.

Buris Han connait l’existence de plusieurs artefacts, dont la Lame Impossible, une arme appelée le Marteau du Temps, et le Sceptre de la Reine. Il a échangé par le passé avec la savante de Barrow Town, **Seniya**, mais il ne lui a pas inspiré confiance et elle lui a refusé son aide.

Il essaiera de convaincre les PJs qu’il peut se joindre à eux et être très utile. Il refusera d’entrer dans la tombe cependant.
Il indiquera aux PJs de se méfier de **Seniya**, disant qu’il y a quelquechose de bizarre en elle, qu’elle semble étrange, perdue, à peine humaine.

### La savante, Seniya

Elle est l’unique savante et historienne de la communauté, tout le monde la connait.

Elle a l’air d’une humaine de 40 ans, cheveux chatains et tâches de rousseur.

Elle est en réalité un être artificiel, servant principalement à collecter de la donnée et faire des recherches, mais elle a reçu des traumatismes qui lui ont fait oublier sa nature et sa mission.

Elle se croit humaine et pense avoir simplement un très grand appétit pour les sciences humaines.

Récemment elle s’est lancée dans des recherches sur l’ancien royaume de **Garamur**.
Elle connait l’emplacement de la Tombe ainsi que les légendes qui l’entourent, notamment :

- La légende d’Aurturi, dieu de la mort. On dit qu’on doit sentir son venin pour s’aventurer au fond du goufre.
- Elle sait que la Reine a été déposée avec la Lame Impossible et son Sceptre.

Seniya n’a aucune compétence martiale, mais il est facile de la convaincre d’accompagner les PJs.

Elle présente une personnalité enjouée, énergique, et très intelligente.    
Sous la surface, elle est épuisée. Ses recherches sont toute sa vie. Elle aimerait se poser mais en est incapable.

### Les Chirani

Parmi les premiers habitants de Barrow-Town, une poignée a découvert un appareil qui suintait un liquide blanc et lumineux.

Ceux qui l’ont touché se sont soudainement retrouvés couverts de cette substance laiteuse.

Ce fluide a changé leur apparence et leur personnalité.

Ils ne mangent plus, ils ne dorment plus, ils coupent toute relation avec les non-Chirani.

Ils ne sont plus les mêmes, bien qu’ils aient conservé leurs mémoires et leurs noms.

Ils vivent toujours parmi les autres humains, mais ils inspirent la méfiance.

Des aggressions de Chirani ont déjà eu lieu, mais il n’est pas établi qu’ils sont tous dangereux.

Il en reste 12 dans la communauté, 8 femmes et 4 hommes.

Ils ont deux pouvoirs secrets des autres humains :

1. Ils peuvent se rendre invisibles,
1. Leur contact peut brûler la chair.

**Chirani** : *Niv 3, au toucher inflige 4 dégâts, peut se rendre invisible jusqu’à une minute par heure.*

## Le voyage vers le goufre

### Avindar le nécromant

Un Nano accompagné d’un Toxodon tirant une charette de bois, chargés de Numenéra divers.  
Il est un homme triste et obsédé par la mort, à la recherche d’un moyen de resusciter son épouse.  
Il s’appelle **Avindar**, et son amour perdu s’appelait **Gerid**.

Avidndar est sur les routes depuis des semaines et il n’a pas le cœur à la discussion.  
Il a l’air fourbu et porte des vêtements de voyage élimés.

Il va à Barrow-Town et il est intéressé par tout objet ayant appartenu à la Reine.
Il ressent beaucoup d’empathie pour **Bach**, l’amant de la reine qui a tout sacrifié pour elle.

Si les PJs le traitent avec gentillesse, il leur donne une copie du code qu’il a trouvé dans un vieux journal à propos de la tombe.

    ABAC BBAA BDCDE


**Avindar :** *Niv 5, Niv 6 pour toute tâche liée aux Numenéra, 1 armure, 15 PV*

## Le Goufre d’Aurturi

Trois tours de métal décrépitent encadre un immense trou, dont le creux a la forme d’un cône de 75m de large.
Profondeur de 55m.
Murs de métal lisse, avec ici et là des traces de corrosion.

Escaliers de bois qui descendent en spirale.
Le bois est ancien, branlant par endroit, ou tout simplement certaines marches manquent.
Au bout de la rampe, une arche ouverte mène au mémorial de la Reine Starloscet.  
Le fond du goufre est encore 300m plus bas.

### Area 1 Le mémorial de la Reine Starloscet (p48)

L’arche ouverte semble contenir une membrane d’énergie, qu’il faut traverser pour entrer.
Cela déclenche l'analyse des créatures qui entrent par la machine de la zone 18, mais aucun moyen de s’en rendre compte.

Le mémorial s’étend sur une trentaine de mètres au-delà de l’arche.
Les murs sont peints de bas-reliefs, âbimés.

Images de la reine dans sa tenue royale, sur son trône, tracté par des esclaves, en plein adoubement d’une femme en armure de plate.

Au dos, image de Starloscet descendant un long conduit menant à une terre d'or.
Un horrible être ressemblant à un insecte l’observe dans sa descente.

Les yeux de la créature sont en réalité des boutons, si poussés, révèlent passage descendant vers une autre chambre.

### Area 2 : L’antre d'Aurturi

Aurturi est un dieu de la mort, dans la mythologie.
En réalité c'est un être bio-ingénieré, insectoïde de taille humaine.

Ses ailes sont trop petites pour lui permettre de voler, son corps est gonflé, et sa large tête à deux yeux à facettes.

6 membres préhensiles, et un terrible pointe au bout de son abdomen.

Il bouge à peine, maladroitement, ses ailes produisent un bruissement sourd.

**Aurturi** Niv5, Niv3 pour tout ce qui touche aux mouvements. 22 PV, 1 armure, son dard inflige 8 dgts si la victime rate un Might DR. Le poison encode une mutation génétique à la victime qui lui permet d’ouvrir les portes des zones 15 et 20.




